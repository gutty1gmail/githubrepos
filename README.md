# README #

#### GibHub Repositories Viewer ####

#### Quick summary ####
This iOS app written in Swift 4.0 (beta) fetches the most trending repositories on GitHub that were created in the last day, last week and last month.
Users are able to easly select the timeframe they would like to visualize, and also are able to sort the repositories by Creation Date (default) or by the numbers of starts.

The main screens shows the following information for each repository:
* User avatar (or default "no avatar" image in case no avatar was selected)
* Username or login
* Repository description (or "no description" in case no description was inserted)
* Number of stars

#Additional Information#
If a selected cell is pressed, the app shows in a new screen basides the already displayed information, the following data:
* Repository language
* Number of forks
* Creation date
* link to the GitHub repository homepage. Pressing the link URL will show the repository homepage in a new screen.

##Additional Features##
* The repositories list in the main screen scrolls with lazy loading mechanims
* Right to Left swapping in a repository cell in Main screen (regardless of the selected period) inserts the repository into the favorites list and shows with an red heart in the repository cell.
* Right to Lect swapping on a favorite repository removes it from the favorites list.
* All the list in Main screen are searchables, the search bar hiddes in the first row if not visible. To reach it, just press the time on the top of the screen.
* In case there is no internet connection, the app will show an alert about it. In case the user selects to remain offline, and favorites list exists, the app will open in favorites list.
* Downloaded Owner's avatar is been cached to avoid unnecesary downloads.


## Sopported Versions ##
iOS 10.0+ for both iPhone and iPad.

### Technical Details ###
## General architecture ##
* The app was developed using MVVM design patern.
* The apps includes 3 views, 1 view model and several models. In addition it includes Misc section with the connection manager, other misc files and extenstion to core classes.
* All network requests are performed in the global queue (background).
* Sort and other heavy performing activities are performed in the global queue (background).
* The app was developed using Xcode 9 beta version to allow using Swift 4.
* Portrate and Landscape mode are supported for all devices.

## Limitations & Known Bugs ##
* No Pods were used to develop the app since was developed in Xcode 9 beta and most of Pods don't fully support iOS11 yet.
* UT were not implemented due to time constraints
* No authorization was implemented for API due to time constraints and the fact that GitHub API allows 60 connections per day without user identification.
* No shared owner between repositories from same owner, meaning each repository has his own ower inforation due to time constraints. In a production implementation, the owner should be shared.
* Owner's avatar cache can be only cleaned by removing the app from background  (or in cases the system kills the app).
* Owner's avatar is not been refreshed in cases the owner changes his avatar in the server during the app runtime.
* swap cell for adding or removing repositories to/from favorites are from the same side and color due to time constraints and the fact the common pods like e.g.: MGSwipeTableCell don't support yet iOS11. Also the remove from favorites can be done only from that list.
* No split view is supported for iPad or iPhone 7+ (in landscape mode) due to time constraints. If additional free time, I would had implemented UISplitViewController.
* Repositories saved in favories are never refreshed while there and are a copy of the repository information at the time of saving.
* Sort by trend doesn't work properly while loading additional repositories (lazy loading). The new records are not been sorted in this version till the sort is re-selected due to time constraints.
