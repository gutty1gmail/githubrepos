//
//  RepoDetailViewController.swift
//  GitHubRepos
//
//  Created by Arie Guttman on 31/08/2017.
//  Copyright © 2017 Arie Guttman. All rights reserved.
//

import UIKit

class RepoDetailViewController: UIViewController {
    
    // MARK: - Storyboard properties
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var loginTitleLbl: UILabel!
    @IBOutlet weak var loginLbl: UILabel!
    
    @IBOutlet weak var startgazersCountLbl: UILabel!
    @IBOutlet weak var descriptionTitleLbl: UILabel!
    @IBOutlet weak var descriptioinLbl: UILabel!
    @IBOutlet weak var languageTitleLbl: UILabel!
    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var forksTitleLbl: UILabel!
    @IBOutlet weak var forksLbl: UILabel!
    @IBOutlet weak var createdAtTitleLbl: UILabel!
    @IBOutlet weak var createdAtLbl: UILabel!
    @IBOutlet weak var linkTitleLbl: UILabel!
    @IBOutlet weak var linkBtn: UIButton!
    
    @IBOutlet weak var linkLbl: UILabel!
    // MARK: - internal properties
    let viewModel = ReposViewModel.sharedInstance
    var selectedRepo: Repository?

    // MARK: - Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initializeController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - View initialization
    ///initialize view controller
    func initializeController()  {
        // Configure title
        self.title = selectedRepo?.repoName
        //customize back button - remove title
        self.navigationItem.backBarButtonItem?.title = ""
        
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        //set lables
        loginTitleLbl.text = "Username:".localized
        loginLbl.text = selectedRepo?.repoOwner.login
        
        startgazersCountLbl.text = "\(selectedRepo?.stargazersCount ?? 0)"
        descriptionTitleLbl.text = "Desc:".localized
        descriptioinLbl.text = selectedRepo?.repoDescription ?? "No description available".localized
        languageTitleLbl.text = "Language:".localized
        languageLbl.text = selectedRepo?.language ?? "No language especified".localized
        forksTitleLbl.text = "Number of forks:".localized
        forksLbl.text = "\(selectedRepo?.forks ?? 0)"
        createdAtTitleLbl.text = "Creation date:".localized
        let localizedDateString = DateFormatter.localizedString(from: (selectedRepo?.createdAt)!, dateStyle: .medium, timeStyle: .short)
        createdAtLbl.text = localizedDateString
        linkTitleLbl.text = "Link:".localized
        linkLbl.text = selectedRepo?.mainUrlString
        self.view.bringSubviewToFront(linkBtn)
    
        
        if !( selectedRepo?.repoOwner.avatarUrl != nil && (selectedRepo?.repoOwner.avatarUrl!.isEmpty)!) {
            viewModel.getAvatarImageForOwner(ownerId: (selectedRepo?.repoOwner.ownerId)!, avatarUrl: (selectedRepo?.repoOwner.avatarUrl!)!) { (image) in
                DispatchQueue.main.async {
                    if let avatarImage = image {
                        UIView.transition(with: self.avatarImageView,
                                          duration: 1,
                                          options: .transitionCrossDissolve,
                                          animations: {
                                            self.avatarImageView.image = avatarImage
                        },
                                          completion: nil)
                        
                    }
                }
            }
        }
        
        
    }
    
    @IBAction func openLink(_ sender: UIButton) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController {
            vc.urlString = selectedRepo?.mainUrlString
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    

}
