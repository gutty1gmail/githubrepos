//
//  MainViewController.swift
//  GitHubRepos
//
//  Created by Arie Guttman on 31/08/2017.
//  Copyright © 2017 Arie Guttman. All rights reserved.
//

import UIKit
import SystemConfiguration

class MainViewController: UIViewController {
    
    // MARK: - Storyboard properties
    
    @IBOutlet weak var periodSegment: UISegmentedControl!
    @IBOutlet weak var sortSegment: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var segmentViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - internal properties
    let viewModel = ReposViewModel.sharedInstance
    ///indicates if requested next page
    var requestedMoreData = false
    
    ///data to present in the view
    var repos = Array<Repository>()
    
    /// search controller
    let searchController = UISearchController(searchResultsController: nil)
    ///filtered repos after search
    var filterRepos = Array<Repository>()
    
    
    // MARK: - Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
          self.initializeController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.activityIndicator.startAnimating()
        
        #if DEBUG
            let startExecuting = Date()
            print("Started selectPeriod in viewWillAppear")
        #endif
        
        //get initial data
        ///used to select period segment when still in main thread
        let periodSegment = RepoSegments(rawValue: self.periodSegment.selectedSegmentIndex)!
        DispatchQueue.global().async {
            self.viewModel.getRepoForSegment(segment: periodSegment) { (newData,repos) in
                DispatchQueue.main.async {
                    if let newRepos = repos {
                        self.repos = newRepos
                        self.sortRepositories(self.sortSegment) { (error) in
                            if let error = error {
                                print("error:\(error.localizedDescription)")
                                #if DEBUG
                                    let endExecuting = Date()
                                    let timeGap = endExecuting.timeIntervalSince(startExecuting)
                                    print("Finish selectPeriod in viewWillAppear handler after \(timeGap) seconds with error ")
                                #endif
                                DispatchQueue.main.async {
                                    self.activityIndicator.stopAnimating()
                                    
                                }
                                
                            } else {
                                DispatchQueue.main.async {
                                    #if DEBUG
                                        let endExecuting = Date()
                                        let timeGap = endExecuting.timeIntervalSince(startExecuting)
                                        print("Finish selectPeriod in viewWillAppear handler after \(timeGap) seconds ")
                                    #endif
                                    self.activityIndicator.stopAnimating()
                                    self.tableView.reloadData()
                                }
                                
                            }
                        }
                        
                    } else {
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            
                        }
                    }
                }

            }
        }

    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .fetchMoreData, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK: - View initialization
    ///initialize view controller
    func initializeController()  {
        // Configure title
        self.title = "GitHub Repositories".localized
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationItem.largeTitleDisplayMode = .automatic
        }
        
        self.edgesForExtendedLayout = .top;
        self.automaticallyAdjustsScrollViewInsets = false;
        
        //configure activity indicator
        self.activityIndicator.style = .whiteLarge
        self.activityIndicator.color = UIColor.gray
        self.view.bringSubviewToFront(activityIndicator)
        activityIndicator.hidesWhenStopped = true
        
        //configure segment
        if !viewModel.areFavoritiesAvailable() {
            periodSegment.removeSegment(at: 3, animated: false)
        }
        
        if !isInternetAvailable() {
           showNoInternetAlert()
        }
        
        //Tableview setup
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ReposTableViewCell", bundle:nil), forCellReuseIdentifier: Constants.CellIdentifiers.reposCell)
 
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        tableView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false

        searchController.searchBar.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.fetchMoreData(_:)), name: .fetchMoreData, object: nil)
        
    }
    
    
    // MARK: - events handlers
    
    @IBAction func selectPeriod(_ sender: UISegmentedControl) {
//        self.requestedMoreData = false
        #if DEBUG
            let startExecuting = Date()
            print("Started selectPeriod segment handler")
        #endif

        tableView.scrollToRow(at: NSIndexPath(row: 0, section: 0) as IndexPath, at: UITableView.ScrollPosition.top, animated: false)
        self.getPeriodData(sender, fetchNewData: false) { (newDataArrived, error) in
            if let error = error {
                print("error:\(error.localizedDescription)")
            } else if let _ = newDataArrived {
                self.sortRepositories(self.sortSegment) { (error) in
                    if let error = error {
                        print("error:\(error.localizedDescription)")
                        #if DEBUG
                            let endExecuting = Date()
                            let timeGap = endExecuting.timeIntervalSince(startExecuting)
                            print("Finish selectPeriod segment handler after \(timeGap) seconds with error ")
                        #endif

                    } else {
                        DispatchQueue.main.async {
                            #if DEBUG
                                let endExecuting = Date()
                                let timeGap = endExecuting.timeIntervalSince(startExecuting)
                                print("Finish selectPeriod segment handler after \(timeGap) seconds ")
                            #endif

                            self.tableView.reloadData()
                        }
                        
                    }
                }
                
            }
            return
        }
        
    }
    
    ///used for background notification selector selector - fetching new data from server
    @objc func getCurrentPeriodData() {
        #if DEBUG
            let startExecuting = Date()
            print("getCurrentPeriodData started from background notification")
        #endif

        self.getPeriodData(self.periodSegment, fetchNewData: true) { (newDataArrived, error) in
            if let error = error {
                print("error:\(error.localizedDescription)")
            } else if let _ = newDataArrived {
                self.sortRepositories(self.sortSegment) { (error) in
                    if let error = error {
                        print("error:\(error.localizedDescription)")
                        #if DEBUG
                            let endExecuting = Date()
                            let timeGap = endExecuting.timeIntervalSince(startExecuting)
                            print("Finish getCurrentPeriodData after \(timeGap) seconds with Error ")
                        #endif

                    } else {
                        DispatchQueue.main.async {
                            #if DEBUG
                                let endExecuting = Date()
                                let timeGap = endExecuting.timeIntervalSince(startExecuting)
                                print("Finish getCurrentPeriodData after \(timeGap) seconds ")
                            #endif

                            self.tableView.reloadData()
                        }
                        
                    }
                }
                
            }
            return
        }
    }
    
    func getPeriodData(_ segment: UISegmentedControl, fetchNewData:Bool,  completion: @escaping (( _ newDataArrived: Bool?, _ error: Error?)->Void)) {
        if !isInternetAvailable()  && segment.selectedSegmentIndex != 3 {
            showNoInternetAlert()
        }
        self.activityIndicator.startAnimating()
        #if DEBUG
            let startExecuting = Date()
            print("Started getPeriodData in Main")
        #endif

        ///used to select period segment when still in main thread
        let periodSegment = RepoSegments(rawValue: segment.selectedSegmentIndex)!
        DispatchQueue.global().async {
            self.viewModel.getRepoForSegment(segment: periodSegment)  { (newData,repos) in
                DispatchQueue.main.async {
//                    if newData! {
//                        self.requestedMoreData = false
//                    }
                    self.activityIndicator.stopAnimating()
                    if let newRepos = repos {
                        self.repos = newRepos
                        #if DEBUG
                            let endExecuting = Date()
                            let timeGap = endExecuting.timeIntervalSince(startExecuting)
                            print("Finish getPeriodData with new \(newRepos.count) repos after \(timeGap) seconds ")
                        #endif

                        completion(newData, nil)
                        return
                        
                    }
                }
            }
        }
    }
    
    @IBAction func sortReposFromScreen(_ sender: UISegmentedControl) {
        sortRepositories(sender) { (error) in
            if let error = error {
                print("error:\(error.localizedDescription)")
            } else {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    return
                }

            }
            return
        }
    }
    
    ///sort repositories based on sorting criteria
    func sortRepositories(_ segment: UISegmentedControl , completion: @escaping (( _ error: Error?)->Void)){
        #if DEBUG
            let startExecuting = Date()
            print("Started sorting arrays")
        #endif

        let reposForSorting: Array<Repository>
        //support filtering
        if isFiltering() {
            reposForSorting = filterRepos
        } else {
            reposForSorting = repos
        }
        ///used to select sort segment when still in main thread
        let sortByDate = SortSegments(rawValue: segment.selectedSegmentIndex)! == .Date
        activityIndicator.startAnimating()
        DispatchQueue.global().async {
            var sortedRepos: Array<Repository>
            if sortByDate {
                sortedRepos = reposForSorting.sorted { $0.createdAt < $1.createdAt }
            } else {
                sortedRepos = reposForSorting.sorted { $0.stargazersCount > $1.stargazersCount }
            }
            
            if self.isFiltering() {
                self.filterRepos = sortedRepos
            } else {
                self.repos = sortedRepos
            }
            
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                #if DEBUG
                    let endExecuting = Date()
                    let timeGap = endExecuting.timeIntervalSince(startExecuting)
                    print("Finish sorting repos after \(timeGap) seconds ")
                #endif

                completion(nil)
                return
            }
            
        }
        
    }
    
    
    // MARK: - Private instance methods
    
    ///get more data notification handler
    @objc func fetchMoreData(_ notification: Notification){
        #if DEBUG
            let startExecuting = Date()
            print("Started notification handler")
        #endif
        guard let userInfo = notification.userInfo else {return}
        guard let period = userInfo[Constants.NotificationInfo.period] as? Period else {return}
        DispatchQueue.global().async {
            self.viewModel.getRepos(period: period, completion: { (newRepos, error) in
                if let error = error {
                    print("error:\(error.localizedDescription)")
                } else if let newRepos = newRepos {
                    
                    let indexPathArray = self.getIndexPathArrayForNewRepos(recordsCount: newRepos.count)
                    self.repos.append(contentsOf: newRepos)
                    DispatchQueue.main.async {
                        self.tableView.beginUpdates()
                        self.tableView.insertRows(at: indexPathArray, with: .bottom)
                        self.tableView.endUpdates()
                        self.requestedMoreData = false
                        #if DEBUG
                            let endExecuting = Date()
                            let timeGap = endExecuting.timeIntervalSince(startExecuting)
                            print("Finish inserting new \(newRepos.count) repos after \(timeGap) seconds ")
                        #endif
                    }
                    
                }
            })
        }

    }
    
    fileprivate func getIndexPathArrayForNewRepos(recordsCount:Int) -> Array<IndexPath> {
        var returnedArray = Array<IndexPath>()
        for i in 0 ..< recordsCount  {
            returnedArray.append(IndexPath(row:self.repos.count + i , section:0))
        }
        return returnedArray
        
    }
    
    fileprivate func periodSegmentToPeriod(segment:RepoSegments) -> Period {
        let periodSegment = RepoSegments(rawValue: self.periodSegment.selectedSegmentIndex)!
        let period: Period
        switch periodSegment {
        case .Day:
            period = .Day
            break
        case .Week:
            period = .Week
            break
        case .Month:
            period = .Week
            
        default:
            period = .Day
            break
        }
        return period
    }
    
    // MARK: - Search related methods
    
    /// Returns true if the text is empty or nil
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    ///filter the repos based on the repo name
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filterRepos = repos.filter({( repo : Repository) -> Bool in
            return repo.repoName.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
     // MARK: - Reachability
    ///check if internet is reachable
    func isInternetAvailable() -> Bool
    {
        //source: https://stackoverflow.com/questions/39558868/check-internet-connection-ios-10
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    ///Alert message
    func showNoInternetAlert(){
        let alertController = UIAlertController(title: "No Internet".localized, message: "This app requires internet connection to function properly, please enable it".localized, preferredStyle: .alert)
        let openNetworkSetting = UIAlertAction(title: "Enable".localized, style: .default) { (action) in
            //support return from background
            self.listernBackgroundNotifications()
            //open app settings to allow user to enable internet
            let settingsUrl = URL(string:UIApplication.openSettingsURLString)
            UIApplication.shared.open(settingsUrl!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
        let dismissAction = UIAlertAction(title: "Remain offline", style: .default) { (action) in
            //
            if self.viewModel.areFavoritiesAvailable() {
                self.periodSegment.selectedSegmentIndex = 3
                self.getCurrentPeriodData()
            } else{
                //support return from background
                self.listernBackgroundNotifications()
                //open app settings to allow user to enable internet
                let settingsUrl = URL(string:UIApplication.openSettingsURLString)
                UIApplication.shared.open(settingsUrl!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
        alertController.addAction(openNetworkSetting)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func listernBackgroundNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.getCurrentPeriodData), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
}

// MARK: - UITableViewDataSource
extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //support filtering
        if isFiltering() {
            return filterRepos.count
        }
        return repos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.reposCell, for: indexPath) as! ReposTableViewCell
        //get repo
        let repo: Repository
        //support filtering
        if isFiltering() {
            repo = filterRepos[indexPath.row]
        } else {
           repo = repos[indexPath.row]
        }
        
        //check if should fetch next page
        if repos.count - indexPath.row < Constants.numberOfReposReminingTillEndOfList && !requestedMoreData && RepoSegments(rawValue: periodSegment.selectedSegmentIndex)! != .Favorites && !isFiltering() {
            
            //request next page
            requestedMoreData = true
            ///used to select period segment when still in main thread
            let period = self.periodSegmentToPeriod(segment: RepoSegments(rawValue: periodSegment.selectedSegmentIndex)! )
            NotificationCenter.default.post(name: .fetchMoreData, object: nil, userInfo: [Constants.NotificationInfo.period : period])
            #if DEBUG
                print("Requested more data from server at row:\(indexPath.row)")
            #endif

            
        }
        
        // Configure the cell...
        cell.loginLbl.text = repo.repoOwner.login
        cell.repoTitleLbl.text = "Name:".localized
        cell.repoNameLbl.text = repo.repoName
        cell.descriptionTitleLbl.text = "Desc:".localized
        cell.descriptioinLbl.text = repo.repoDescription ?? "No description available".localized
        cell.startgazersCountLbl.text = "\(repo.stargazersCount)"
        
        cell.favoriteImage.isHidden = !self.viewModel.doesRepoExistInFavorites(repoId: repo.repoId)
        
        //configure avatar
        if !( repo.repoOwner.avatarUrl != nil && repo.repoOwner.avatarUrl!.isEmpty) {
            viewModel.getAvatarImageForOwner(ownerId: repo.repoOwner.ownerId, avatarUrl: repo.repoOwner.avatarUrl!) { (image) in
                DispatchQueue.main.async {
                    if let avatarImage = image {
                            UIView.transition(with: cell.avatarImageView,
                                              duration: 1,
                                              options: .transitionCrossDissolve,
                                              animations: {
                                               cell.avatarImageView.image = avatarImage
                            },
                                              completion: nil)
                        
                    }
                }
            }
        }
   
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var actions = Array<UITableViewRowAction>()
        if  RepoSegments(rawValue: periodSegment.selectedSegmentIndex)! != .Favorites {
            let insertAction = UITableViewRowAction(style: .default, title: "Add to Favorites".localized, handler: { (_, indexPath) in
                
                let selectedRepo: Repository
                //support filtering
                if self.isFiltering() {
                    selectedRepo = self.filterRepos[indexPath.row]
                } else {
                    selectedRepo = self.repos[indexPath.row]
                }
                
                //check if repo was already added to favorites
                if !self.viewModel.doesRepoExistInFavorites(repoId: selectedRepo.repoId) {
                    //add repo to view model favorites DB
                    self.viewModel.addRepoToFavorites(repo: selectedRepo)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    if self.periodSegment.numberOfSegments == 3 {
                        //add favorites to segment
                        self.periodSegment.insertSegment(withTitle: "Fav".localized, at: 3, animated: true)
                    }
                }
                
            })
            actions.append(insertAction)
        } else if  RepoSegments(rawValue: periodSegment.selectedSegmentIndex)! == .Favorites {
            let deleteAction = UITableViewRowAction(style: .destructive, title: "Remove".localized, handler: { (_, indexPath) in
                //remove the repo from favorites
                self.viewModel.removeRepoFromFavorites(at: indexPath.row)
                
                //support filtering
                if self.isFiltering() {
                   self.filterRepos.remove(at: indexPath.row)
                } else {
                    self.repos.remove(at: indexPath.row)
                }
                tableView.deleteRows(at: [indexPath], with: .fade)
                if !self.viewModel.areFavoritiesAvailable(){
                    //remove favories segment since no repos remain in favorites
                    self.periodSegment.selectedSegmentIndex = 0
                    self.periodSegment.removeSegment(at: 3, animated: true)
                    self.getPeriodData(self.periodSegment, fetchNewData: false, completion: { (newDataArrived, error) in
                        if let error = error {
                            print("error:\(error.localizedDescription)")
                        } else if let _ = newDataArrived {
                            self.sortRepositories(self.sortSegment) { (error) in
                                if let error = error {
                                    print("error:\(error.localizedDescription)")
                                } else {
                                    DispatchQueue.main.async {
                                        self.tableView.reloadData()
                                    }
                                    
                                }
                            }
                            
                        }
                    })
                    
                }
            })
            actions.append(deleteAction)
        }
        
        return actions
    }
    
}

// MARK: - UITableViewDataSource
extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repo: Repository
        //support filtering
        if isFiltering() {
            repo = filterRepos[indexPath.row]
        } else {
            repo = repos[indexPath.row]
        }
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RepoDetailViewController") as? RepoDetailViewController {
            vc.selectedRepo = repo
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
   
}


// MARK: - UISearchResultsUpdating
extension MainViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    
    
}

 // MARK: - UISearchBar Delegate
extension MainViewController: UISearchBarDelegate {
   
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        UIView.transition(with: segmentView,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.segmentView.isHidden = true
                            self.segmentViewHeightConstraint.constant = 0
        },
                          completion: nil)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        UIView.transition(with: segmentView,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.segmentView.isHidden = true
                            self.segmentViewHeightConstraint.constant = 0
        },
                          completion: nil)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        UIView.transition(with: segmentView,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.segmentView.isHidden = false
                            self.segmentViewHeightConstraint.constant = 64
        },
                          completion: nil)
    }
    
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
