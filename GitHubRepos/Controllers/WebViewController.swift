//
//  WebViewController.swift
//  GitHubRepos
//
//  Created by Arie Guttman on 01/09/2017.
//  Copyright © 2017 Arie Guttman. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    // MARK: - Storyboard properties
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var loadSpinner: UIActivityIndicatorView!
    
    ///Contains the url to be open in string format
    var urlString:String?
   
    // MARK: - Controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        webView.delegate = self
        loadSpinner.hidesWhenStopped = true
        
        //customize back button - remove title
         self.navigationItem.backBarButtonItem?.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        // Configure title
        self.title = "Repository Homepage"
        
        //configure activity indicator
        self.loadSpinner.style = .whiteLarge
        self.loadSpinner.color = UIColor.gray
        self.view.bringSubviewToFront(loadSpinner)
        loadSpinner.hidesWhenStopped = true
        
        // Do any additional setup after loading the view.
        guard let urlString =  urlString else {
            back()
            return
        }
        guard let url = URL(string: urlString) else {
            back()
            return
        }
        let urlRequest = URLRequest(url: url)
        webView.loadRequest(urlRequest)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func back() {
        self.navigationController?.popViewController(animated: true)
    }

}

extension WebViewController: UIWebViewDelegate {
    func webViewDidStartLoad(_ : UIWebView) {
        loadSpinner.startAnimating()
    }
    
    func webViewDidFinishLoad(_ : UIWebView) {
        loadSpinner.stopAnimating()
    }
}
