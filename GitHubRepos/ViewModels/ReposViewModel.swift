//
//  ReposViewModel.swift
//  GitHubRepos
//
//  Created by Arie Guttman on 29/08/2017.
//  Copyright © 2017 Arie Guttman. All rights reserved.
//

import Foundation
import UIKit

/// for singleton use in ReposViewModel
private let sharedRposViewModel = ReposViewModel()

///Supported repos period segments
enum RepoSegments: Int {
    case Day = 0
    case Week
    case Month
    case Favorites
}

//Supported repos sort segments
enum SortSegments: Int {
    case Date = 0
    case Trend
}

class ReposViewModel{
    
    ///Singleton implementation for ReposViewModel
    class var sharedInstance: ReposViewModel {
         return sharedRposViewModel
    }
    
    //MARK: - Internal variables
    /// used to show the array in the view
    fileprivate var showingRepos:Array<Repository>
    ///Contains the last day repos
    fileprivate var lastDayRepos:Array<Repository>
    ///Contains the last week repos
    fileprivate var lastWeekRepos:Array<Repository>
    ///Contains the last month repos
    fileprivate var lastMonthRepos:Array<Repository>
    ///User Favorites repositories
    fileprivate var favoritesRepos:Array<Repository>
    ///Owner avatar cache
    fileprivate var ownerAvatars: Array<OwnerAvatar>
    
    fileprivate var pages : Array<Page>
    
    //declare the defaults
    let defaults:UserDefaults = UserDefaults.standard
    //MARK: - Implementation
    
    init() {
        showingRepos = Array<Repository>()
        lastDayRepos = Array<Repository>()
        lastWeekRepos = Array<Repository>()
        lastMonthRepos = Array<Repository>()
        favoritesRepos =  Array<Repository>()
        ownerAvatars = Array<OwnerAvatar>()
        pages = Array<Page>()
        favoritesRepos = self.retrieveFavoritesFromDefaults()
        ownerAvatars = self.retrieveOwnerAvatarFromDefaults()
        self.initializePages()
        
    }
    /// pages structure initialization
    fileprivate func initializePages(){
        pages = Array<Page>()
        let day = Page(period: .Day)
        pages.append(day)
        let week = Page(period: .Week)
        pages.append(week)
        let month = Page(period: .Month)
        pages.append(month)
    }
    
    ///get page for given period
    fileprivate func getPage(period:Period)->Page {
        return (pages.filter{$0.period == period}).first!
        
    }
    
    ///update Page in Collection
    fileprivate func updatePageInCollection (page: Page) {
        //get all page
        let oldPage = self.getPage(period: page.period)
        //remove all page
        if let itemToRemoveIndex = pages.firstIndex(of: oldPage) {
            pages.remove(at: itemToRemoveIndex)
        }
        //add updated page
        pages.append(page)
    }
    
    /// get favorites from defaults or create an empty array
    fileprivate func retrieveFavoritesFromDefaults() -> Array<Repository> {
        guard let favoritesData = defaults.object(forKey: Constants.UserDefaultsString.favorites) as? Data  else {
            return self.initFavorites()
        }
        guard let favorites = NSKeyedUnarchiver.unarchiveObject(with: favoritesData) as? Array<Repository>  else {
            return self.initFavorites()
        }
        return favorites
    }
    
    ///Set an emptly favorites array and archive it inside defaults
    fileprivate func initFavorites() -> Array<Repository>{
        //initialize emty favorites
        let initializedFavorites = Array<Repository>()
        //Save initialized array into defaults
        self.saveArrayIntoDefaults(collection: initializedFavorites, forKey: Constants.UserDefaultsString.favorites)
        
        return initializedFavorites
    }
    
    ///Save array into Defaults
    fileprivate func saveArrayIntoDefaults(collection:Array<Any>, forKey:String ) {
        //archive in defaults
        let favoritesData = NSKeyedArchiver.archivedData(withRootObject: collection)
        defaults.set(favoritesData, forKey: forKey)
        defaults.synchronize()
    }
    
    
    /// get Owner Avatar from defaults or create an empty array
    fileprivate func retrieveOwnerAvatarFromDefaults() -> Array<OwnerAvatar> {
        guard let favoritesData = defaults.object(forKey: Constants.UserDefaultsString.ownerAvatar) as? Data  else {
            return self.initOwnerAvatar()
        }
        guard let favorites = NSKeyedUnarchiver.unarchiveObject(with: favoritesData) as? Array<OwnerAvatar>  else {
            return self.initOwnerAvatar()
        }
        return favorites
    }
    
    ///Set an emptly favorites array and archive it inside defaults
    fileprivate func initOwnerAvatar() -> Array<OwnerAvatar>{
        //initialize emty favorites
        let initializedFavorites = Array<OwnerAvatar>()
        //Save initialized array into defaults
        self.saveArrayIntoDefaults(collection: initializedFavorites, forKey: Constants.UserDefaultsString.ownerAvatar)
        return initializedFavorites
    }
    
    ///get from API the repos for the requested period
    fileprivate func getDataFromAPI(period:Period, completion: @escaping (( _ jsonObject:JsonAssociatedData?, _ error: Error?)->Void)) {
        ///Singleton implementation for ConnectionManager
        let connection = ConnectionManager.sharedInstance
        
        ///URL in string format for the connection
        let postURL = Constants.hostAPI + "/search/repositories"
        
        
        //calculate date based on period
        let now = Date()
        
        let requestedDate:Date
        
        //check if last page already fetched for period
        var page = self.getPage(period: period)
        if page.lastPageFetched() {
            completion(nil, iError.AllPagesArrived)
            return
        }
        ///page to be requested
        let requestedPage = page.getPageNumberToFetch()

        switch period {
        case .Week:
             requestedDate = now.addDays(days: -7)
            break
        case .Month:
            requestedDate = now.addMonths(months: -1)
            break
        default:
            //Day is the assigned default value
             requestedDate = now.addDays(days: -1)
         }
        
        var params = Dictionary<String,String>()
        params["q"] = "created:>\(requestedDate.asQueryDate!)"
        params["sort"] = "stars"
        params["order"] = "desc"
        params["page"] = "\(requestedPage)"

        connection.GET_Request(urlString: postURL, header: Dictionary<String,String>(), restParameters: params) { (data, error) in
            if let error = error {
                completion(nil, error)
                return
            } else {
                if let data = data {
                    //parse the json
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    do{
                        let jsonParsed = try decoder.decode(JsonAssociatedData.self, from: data)
                        
                            //update paging mechanims for next fetch
                        if jsonParsed.incompleteResults {
                            page.setLastPage()
                        }
                        else {
                            page.incrementPage()
                        }
                        //refresh collection with the updated page
                        self.updatePageInCollection(page: page)
                        
                        completion(jsonParsed, nil)
                        return
                    } catch let jsonError {
                        completion(nil, jsonError)
                        return
                    }
                } else {
                    completion(nil, iError.RuntimeError(reason: "Wrong Data returned from Server".localized))
                    return
                }
            }
        }
        
    }
    
     func getRepos(period:Period, completion: @escaping ((_ newData:Array<Repository>?,_ error: Error?)->Void))  {
        //Get data from server
        self.getDataFromAPI(period: period) { (jsonParsedObject, error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
                completion(nil,error)
                return
            } else {
                if let newRepos = jsonParsedObject?.repos  {
                    switch period {
                    case .Week:
                        self.lastWeekRepos.append(contentsOf: newRepos)
                    case .Month:
                        self.lastMonthRepos.append(contentsOf: newRepos)
                    default:
                        //Day is the assigned default value
                        self.lastDayRepos.append(contentsOf: newRepos)
                    }
                    
                    //update caller that new data arrived
                    completion(newRepos,nil)
                    return
                    
                } else {
                    //no data arrived
                    completion(nil,iError.NoDataArrived)
                    return
                }
            }
        }
    }
    
    ///gets the repository data for the selected segment
    func getRepoForSegment(segment:RepoSegments, completion: @escaping ((_ newDataArrived: Bool?,_ repos: Array<Repository>?)->Void)) {
        switch segment {
        case .Day:
            if lastDayRepos.isEmpty  {
                self.getRepos(period: .Day, completion: { (newData,error) in
                    if let error = error {
                        print("error:\(error.localizedDescription)")
                        completion( false,nil)
                        return
                    } else {
                        completion(true,newData)
                        return
                    }
                })
            } else {
                completion(false,self.lastDayRepos)
                return
            }
            break
        case .Week:
            if lastWeekRepos.isEmpty {
                self.getRepos(period: .Week, completion: { (newData,error) in
                    if let error = error {
                        print("error:\(error.localizedDescription)")
                        completion(false,nil)
                        return
                    } else {
                        completion(true,newData)
                        return
                    }
                })
            } else {
                completion(false,self.lastWeekRepos)
                return
            }
            break
        case .Month:
            if lastMonthRepos.isEmpty  {
                self.getRepos(period: .Month, completion: { (newData,error) in
                    if let error = error {
                        print("error:\(error.localizedDescription)")
                        completion(false,nil)
                        return
                    } else {
                        completion(true,newData)
                        return
                    }
                })
            } else {
                completion(false,self.lastMonthRepos)
                return
            }
            
        case .Favorites:
            completion(false,self.favoritesRepos)
            return
            
        }
    }
    
    ///indicates if user saved favorites repos
    func areFavoritiesAvailable() -> Bool {
        return favoritesRepos.count > 0 ? true:false
    }
    
    ///
    /**
     
     get avatar in the repo's owner.
     
     This method get the owner avatar image from cache or server with the provided input parameters.
     
     - ownerId: Int - owner id from repo.
     - avatarUrl:String - the avatar url as especified in the owner object.
     - completion:ResponseBlock - ((_ image: UIImage?)->Void)) - the existing or downloded image
     - Returns:   none.
     */
    func getAvatarImageForOwner( ownerId: Int, avatarUrl:String, completion: @escaping ((_ image: UIImage?)->Void))  {
        if ownerAvatars.contains(where: { $0.ownerId == ownerId }) {
            // found
            let ownerAvatar = (ownerAvatars.filter{$0.ownerId == ownerId}).first!
            completion(ownerAvatar.avatarImage)
            return
        } else {
            // not found - get image and store it in cache
            self.downloadFromURL(urlString: avatarUrl, callback: { (image, error) in
                if let error = error {
                    print("Error in imageURL: \(avatarUrl) - Error:\(error.localizedDescription)")
                } else {
                    if let returnedImage = image {
                        //image arrived, get a new OwnerAvatar object and added to cache before returning it
                        let ownerAvatar = OwnerAvatar(ownerId: ownerId)
                        ownerAvatar.avatarImage = returnedImage
                       
                        //save the updated cache collection in userDefaults for future use
                         self.ownerAvatars.append(ownerAvatar)
//                        self.saveArrayIntoDefaults(collection: self.ownerAvatars, forKey: Constants.UserDefaultsString.ownerAvatar)
                        completion(returnedImage)
                        return
                    }
                }
            })
            
        }
    }
    
    fileprivate func downloadFromURL(urlString:String, callback:((_ image:UIImage?, _ error: Error?)->Void)?)   {
        var image: UIImage?
        guard let url = URL(string: urlString) else {
            callback!(nil, iError.RuntimeError(reason: "Wrong URL for image".localized))
            return
        }
        DispatchQueue.global(qos: .userInitiated).async {
            #if DEBUG
                let startExecuting = Date()
            #endif
            do {
                let data = try Data(contentsOf: url)
                image = UIImage(data: data)
                DispatchQueue.main.async {
                    callback!(image,nil)
                    return
                }
                
            } catch {
                print(error.localizedDescription)
                #if DEBUG
                    let endExecuting = Date()
                    let timeGap = endExecuting.timeIntervalSince(startExecuting)
                    print("Finish downloading image with error after \(timeGap) seconds ")
                #endif

                callback!(nil, iError.RuntimeError(reason: "Data error for image".localized))
                return
                
            }
        }
    }
    
    ///remove an entry from favorites on a given index
    func removeRepoFromFavorites(at:Int) {
        self.favoritesRepos.remove(at: at)
        self.saveArrayIntoDefaults(collection: self.favoritesRepos, forKey: Constants.UserDefaultsString.favorites)
    }
    
    ///add a repo to favorites
    func addRepoToFavorites(repo: Repository) {
        self.favoritesRepos.append(repo)
        self.saveArrayIntoDefaults(collection: self.favoritesRepos, forKey: Constants.UserDefaultsString.favorites)
        
    }
    
    ///indicates the current repository already exists in the favorites repository
    func doesRepoExistInFavorites(repoId: Int) -> Bool {
        return favoritesRepos.contains(where: { $0.repoId == repoId })
    }
}
