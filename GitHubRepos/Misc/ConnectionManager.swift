//
//  ConnectionManager.swift
//  GitHubRepos
//
//  Created by Arie Guttman on 29/08/2017.
//  Copyright © 2017 Arie Guttman. All rights reserved.
//

import Foundation

typealias ResponseDataBlock = ((_ data:Data?, _ error: Error?)->Void)


/// for singleton use in ConnectionManager
private let sharedConnectionManager = ConnectionManager()


class ConnectionManager {
    
    
    ///Singleton implementation for ConnectionManager
    class var sharedInstance: ConnectionManager {
        return sharedConnectionManager
    }

    
    /**
     
     Executes a REST GET call.
     
     This method executes a REST GET call with the provided input parameters.
     
     - Parameters:
     - urlString:String - URL string.
     - header:Dictionary<String,String> - Dictionary with the header parameters for the call.
     - restParameters:Dictionary<String,String> - Dictionary with the parameters for the call.
     - callback:ResponseBlock - ((_ jsonData:Any, _ error: NSError?)->Void)
     - Returns:   none.
     */
    func GET_Request(urlString:String,  header:Dictionary<String,String>, restParameters:Dictionary<String,String>, callback:@escaping ResponseDataBlock) {
        //prepare the request
        guard URL(string: urlString) != nil else {
            callback(nil, iError.RuntimeError(reason: "Wrong URL".localized))
            return
        }
        //build full URL string
        let parameterString = restParameters.stringFromHttpParameters()
        let fullURLString = "\(urlString)?\(parameterString)"
        
        //prepare request
        let requestURL = URL(string:fullURLString)!
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        
        //HTTP Headers
        for (key,value) in header {
            request.addValue(key, forHTTPHeaderField:value)
        }
        //        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = URLSession.shared.dataTask(with: request)  {
            ( data: Data?, response: URLResponse?, error: Error?) in
            do {
                if let error = error {
                    callback(nil,error)
                    return
                }
                
                //handle response code
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        switch httpResponse.statusCode {
                        case 404 ,401 :
                            //since Not Found = 404 error for this server actually means Unauthorized - 401
                            callback(nil,iError.AuthorizationFailure)
                            return
                        default:
                            //get error response message from server
                            let parsedData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! Dictionary<String,Any>
                            
                            if let internalResponseDictionary = parsedData["ErrorResponseEntity"] as? Dictionary<String,Any> {
                                if let errorDescriptionFromServer = internalResponseDictionary["errorMessage"] as? String {
                                    
                                    callback(nil,iError.RuntimeError(reason: errorDescriptionFromServer))
                                    return
                                }
                            }
                            break
                            
                            
                        }

                        
                    }
                }
                
                if data != nil  {
                    callback(data,nil)
                    return
                }
                
                
            } catch let jsonError {
                callback(nil, jsonError)
                return
            }
        }
        task.resume()
        
    }
}
