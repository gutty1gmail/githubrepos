//
//  Definitions.swift
//  GitHubRepos
//
//  Created by Arie Guttman on 29/08/2017.
//  Copyright © 2017 Arie Guttman. All rights reserved.
//

import Foundation

///Supported period types
enum Period {
    case Day
    case Week
    case Month
}

