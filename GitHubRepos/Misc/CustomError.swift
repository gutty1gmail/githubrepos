//
//  CustomError.swift
//  GitHubRepos
//
//  Created by Arie Guttman on 29/08/2017.
//  Copyright © 2017 Arie Guttman. All rights reserved.
//

import Foundation


protocol OurErrorProtocol: Error {
    
    var localizedTitle: String { get }
    var localizedDescription: String { get }
    var code: Int { get }
}

struct CustomError: OurErrorProtocol {
    
    var localizedTitle: String
    var localizedDescription: String
    var debugDescription: String
    var code: Int
    
    init(localizedTitle: String?, localizedDescription: String, code: Int, debugDescription:String) {
        self.localizedTitle = localizedTitle ?? "Error"
        self.localizedDescription = localizedDescription
        self.debugDescription = debugDescription
        self.code = code
    }
}


enum iError: Error {
    case UnknownError(reason: String)
    case ConnectionError
    case InvalidCredentials
    case InvalidRequest
    case NotFound
    case AuthorizationFailure
    case InvalidResponse(reason: String)
    case ServerError
    case ServerUnavailable
    case TimeOut
    case UnsuppotedURL
    case NoDataArrived
    case AllPagesArrived
    case UnsuccessfullResponse(reason: String)
    case RuntimeError(reason: String)
}

