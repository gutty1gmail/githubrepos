//
//  StringExtension.swift
//  GitHubRepos
//
//  Created by Arie Guttman on 29/08/2017.
//  Copyright © 2017 Arie Guttman. All rights reserved.
//

import Foundation


extension String {
    
    ///returns a localized string without comment
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    ///localized string with comment
    func localizedWithComment(comment:String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: comment)
    }
    
    /// add percentage encoding to Strings for URL construction
    func addingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }
    
}
