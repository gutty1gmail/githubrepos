//
//  ReposTableViewCell.swift
//  GitHubRepos
//
//  Created by Arie Guttman on 31/08/2017.
//  Copyright © 2017 Arie Guttman. All rights reserved.
//

import UIKit

class ReposTableViewCell: UITableViewCell {
    
    // MARK: - Storyboard properties
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var loginLbl: UILabel!
    @IBOutlet weak var repoTitleLbl: UILabel!
    @IBOutlet weak var repoNameLbl: UILabel!
    
    @IBOutlet weak var startgazersCountLbl: UILabel!
    @IBOutlet weak var descriptionTitleLbl: UILabel!
    @IBOutlet weak var descriptioinLbl: UILabel!
    @IBOutlet weak var favoriteImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
