//
//  ConnectionManagerTests.swift
//  GitHubReposTests
//
//  Created by Arie Guttman on 05/09/2017.
//  Copyright © 2017 Arie Guttman. All rights reserved.
//

import XCTest

class ConnectionManagerTests: XCTestCase {
    //Singleton implementation for ConnectionManager
    var connection = ConnectionManager.sharedInstance
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testConnectionWithWrongURL() {
        ///URL in string format for the connection
        let postURL = Constants.hostAPI + "/search/repositori"
        var params = Dictionary<String,String>()
        
        params["q"] = "created:>2017-06-01"
        params["sort"] = "stars"
        params["order"] = "desc"
        params["page"] = "1"
        connection.GET_Request(urlString: postURL, header: Dictionary<String,String>(), restParameters: params) { (data, error) in
            if let error = error {
                XCTAssertNil(error, "expected error message didn't arrive")
                return
            } else {
                if let data = data {
                    XCTAssertNotNil(data, "expected data to be nil")
                }
            }
        }
        
    }
    
    
    func testConnectionWithCorrenctURL() {
        ///URL in string format for the connection
        let postURL = Constants.hostAPI + "/search/repositories"
        var params = Dictionary<String,String>()
        
        params["q"] = "created:>2017-06-01"
        params["sort"] = "stars"
        params["order"] = "desc"
        params["page"] = "1"
        connection.GET_Request(urlString: postURL, header: Dictionary<String,String>(), restParameters: params) { (data, error) in
            if let error = error {
                XCTAssertNotNil(error, "unexpected error message arrived")
                return
            } else {
                if let data = data {
                    XCTAssertNil(data, "expected data didn't arrive")
                }
            }
        }
        
    }
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
